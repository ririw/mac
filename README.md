# mac

Implementation of Memory, Attention, and Composition paper

# How to run this code:

 1. Clone the code
 2. Install it: `python setup.py develop`
 3. Download the [CLEVR dataset](https://cs.stanford.edu/people/jcjohns/clevr/)
 4. Extract it somewhere... somewhere with a lot of space
 5. Run `mac-learn train <path to CLEVR> <preprocessing location> <results location> <logs location>`
    - The preprocessing location should be some empty directory with a lot of space
    - The results location should also be an empty directory
    - The logs location is optional. If passed, tensorboard logs will be written to it
  